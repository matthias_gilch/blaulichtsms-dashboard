# BlaulichtSMS API

This module implements the API of BlaulichtSMS. The original API is described here: [https://github.com/blaulichtSMS/docs](https://github.com/blaulichtSMS/docs)

# [BlaulichtSMS](https://blaulichtsms.net/)

BlaulichtSMS is a service providing secondary callouts for emergency services. 
